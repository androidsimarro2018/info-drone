package crono.com.infodrone;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;

import crono.com.infodrone.controller.ControllerBD;
import crono.com.infodrone.controller.MyCallBack;
import crono.com.infodrone.controller.TiendaAdapter;
import crono.com.infodrone.models.Producto;

public class MyProductsActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private ArrayList<Producto> listaProductos = new ArrayList<>();
    private TiendaAdapter<Producto> adapter;
    private GridView grdProducts;
    private ControllerBD controllerBD = new ControllerBD();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_tienda);
        setContentView(R.layout.activity_tienda);

        grdProducts = findViewById(R.id.grdProducts);
        grdProducts.setOnItemClickListener(this);

        listaProductos = controllerBD.leerBasedeDatosMisProductos(new MyCallBack() {
            @Override
            public void onCallBackProducto(ArrayList<Producto> list) {

                adapter = new TiendaAdapter<Producto>(getApplicationContext(), listaProductos);

                grdProducts.setAdapter(adapter);

            }

        });

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intentProduct = new Intent(this, myProductClickActivity.class);
        intentProduct.putExtra("id", listaProductos.get(position).getId());
        intentProduct.putExtra("price", listaProductos.get(position).getPrice());
        intentProduct.putExtra("tittle", listaProductos.get(position).getTitle());
        intentProduct.putExtra("description", listaProductos.get(position).getDescription());
        intentProduct.putExtra("phone", listaProductos.get(position).getPhone());
        intentProduct.putExtra("encoded", listaProductos.get(position).getEncoded());
        startActivity(intentProduct);
        finish();
    }

}
