package crono.com.infodrone;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.Guideline;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import com.facebook.CallbackManager;
import com.facebook.login.widget.LoginButton;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.Locale;

import crono.com.infodrone.models.User;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener  {

    private Button btnAcceder;
    private FirebaseAuth firebaseAuth;
    private TextView txtNuevoUsuario;
    private TextView txtUser;
    private TextView txtPassword;
    private TextView txtForgotPassword;
    private String user;
    private String password;

    private CallbackManager callbackManager;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private TextView txtUserFaceook;
    private TextView txtEmail;
    private ImageView imgProfile;
    private LoginButton logoutButton;
    private LoginButton loginButton;

    private ConstraintLayout c;

    private Guideline gl1;

    private ConstraintLayout.LayoutParams parametro;

    DatabaseReference reference;
    FirebaseStorage storage;
    StorageReference storageReference;
    private FirebaseUser firebaseUser;
    private String userId;
    private User newUser;
    private String namePreferences;
    private String lastNamePreferences;
    private String userPreferences;

    private String emailEnviado;
    String idioma = "hola";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        idioma = Locale.getDefault().getDisplayLanguage();

        if(idioma.equals("English")){ // Idioma del movil en ingles
            guardarPreferenciasIdioma("us");
        }else{ // Idioma del movil en español
            guardarPreferenciasIdioma("mx");
        }

        emailEnviado = leerPreferencias();

        if(!emailEnviado.equals("")){
            Toast.makeText(this, emailEnviado, Toast.LENGTH_LONG).show();
            guardarPreferencias();
        }

        txtUser = findViewById(R.id.txtUsuario);
        txtPassword = findViewById(R.id.txtPassword);
        btnAcceder = findViewById(R.id.btnSendNewPassword);
        txtForgotPassword = findViewById(R.id.txtOlvidarPassword);

        btnAcceder.setOnClickListener(this);

        txtNuevoUsuario = findViewById(R.id.txtNuevoUsuario);

        txtNuevoUsuario.setOnClickListener(this);
        txtForgotPassword.setOnClickListener(this);

        c = findViewById(R.id.layoutOcultar);
        gl1 = findViewById(R.id.guideline);
        parametro = (ConstraintLayout.LayoutParams) gl1.getLayoutParams();
        parametro.guidePercent = 0.55f;

        firebaseAuth = FirebaseAuth.getInstance();

        //firebaseAuth.addAuthStateListener(mAuthListener); Eso es per al login de facebook

        //MiBD miBD = MiBD.getInstance(getApplicationContext());
        //SQLiteDatabase db = miBD.getWritableDatabase();

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btnSendNewPassword: // Botón de Acceder

                if (txtUser.getText().toString().equals("") || txtPassword.getText().toString().equals("")) { // USUARIO O CONTRASEÑA VACÍA

//                    user = "tomas@gmail.com";
//                    password = "123456";

                    c.setVisibility(View.VISIBLE);
                    parametro.guidePercent = 0.59f;
                    gl1.setLayoutParams(parametro);

                } else {

                    user = txtUser.getText().toString();
                    password = txtPassword.getText().toString();

//                    user = "tomas@gmail.com";
//                    password = "123456";

                    firebaseAuth.signInWithEmailAndPassword(user, password)
                            .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {

                                    if (task.isSuccessful()) { // Usuario y password CORRECTO, pasamos al main
                                        finish();
                                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                        startActivity(intent);

                                    } else { // ERROR DE USUARIO O CONTRASEÑA, INFORMAMOS DEL ERROR

                                        c.setVisibility(View.VISIBLE);
                                        parametro.guidePercent = 0.59f;
                                        gl1.setLayoutParams(parametro);

                                    }

                                }
                            });
                }

                break;

            case R.id.txtNuevoUsuario: // Nuevo Usuario

                Intent intentNew = new Intent(this, NewUser.class);
                startActivity(intentNew);
                finish();

                break;

            case R.id.txtOlvidarPassword: // Olvidar Contraseña

                Intent intentForgotPassword = new Intent(this, ForgotPasswordActivity.class);
                startActivity(intentForgotPassword);
                finish();

                break;

        }

    }

    /*
    public void loginFacebook() {

        FacebookSdk.sdkInitialize(getApplicationContext());
        //AppEventsLogger.activateApp(this);
        setContentView(R.layout.activity_login_facebook);
        txtUserFaceook = (TextView) findViewById(R.id.txtUser);
        txtEmail = (TextView) findViewById(R.id.txtEmail);
        imgProfile = (ImageView) findViewById(R.id.imgProfile);
        firebaseAuth = FirebaseAuth.getInstance();
        callbackManager = CallbackManager.Factory.create();

        loginButton = (LoginButton) findViewById(R.id.btnFacebookIn);
        logoutButton = (LoginButton) findViewById(R.id.btnFacebookOut);
        logoutButton.setOnClickListener(this);


        loginButton.setReadPermissions("email", "public_profile");//user_status, publish_actions..
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Toast.makeText(LoginActivity.this, "Sign In canceled", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(LoginActivity.this, "Something bad happened", Toast.LENGTH_SHORT).show();
            }
        });

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (AccessToken.getCurrentAccessToken() != null)
                    Toast.makeText(LoginActivity.this, AccessToken.getCurrentAccessToken().getExpires().toString(), Toast.LENGTH_SHORT).show();
                if (user != null) {
                    String email = user.getEmail();
                    String userName = user.getDisplayName();
                    txtEmail.setText(email);
                    txtUser.setText(userName);
                    //Picasso.with(LoginActivity.this).load(user.getPhotoUrl()).into(imgProfile);
                    loginButton.setVisibility(View.GONE);
                    logoutButton.setVisibility(View.VISIBLE);
                } else {
                    Log.d("TG", "SIGNED OUT");
                    txtEmail.setText("");
                    txtUser.setText("");
                    imgProfile.setImageBitmap(null);
                    loginButton.setVisibility(View.VISIBLE);
                    logoutButton.setVisibility(View.GONE);
                }
            }
        };
    }*/


    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();
    }


    @Override
    public void onBackPressed() {
        finish();
        moveTaskToBack(true); // esta línea permet tancar la app si es tira arrere estiguent en el login
    }

    public String leerPreferencias(){

        SharedPreferences preferences = getSharedPreferences("preferences", Context.MODE_PRIVATE);

        return preferences.getString("emailEnviado","");

    }

    public void guardarPreferencias(){
        SharedPreferences preferences = getSharedPreferences("preferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString("emailEnviado", "");
        editor.commit();
    }

    public void guardarPreferenciasIdioma(String idioma){
        SharedPreferences preferences = getSharedPreferences("preferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString("idioma", idioma);
        editor.commit();

    }

}

