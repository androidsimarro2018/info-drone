package crono.com.infodrone;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class LeyesActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView imageViewSeguridad;
    private ImageView imageViewBOE;
    private ImageView imageViewGobierno;
    private ImageView imageViewAESA;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leyes);

        imageViewSeguridad = findViewById(R.id.imgSeguridad);
        imageViewBOE = findViewById(R.id.imgBOE);
        imageViewGobierno = findViewById(R.id.imgGobierno);
        imageViewAESA = findViewById(R.id.imgAESA);

        imageViewSeguridad.setOnClickListener(this);
        imageViewBOE.setOnClickListener(this);
        imageViewGobierno.setOnClickListener(this);
        imageViewAESA.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imgSeguridad: // Seguridad Aérea

                Intent intentSeguridad = new Intent(this, LeySeguridad.class);
                startActivity(intentSeguridad);

                break;

            case R.id.imgBOE: // BOE

                Intent intentBOE = new Intent(this, LeyBOE.class);
                startActivity(intentBOE);

                break;

            case R.id.imgGobierno: // Gobierno

                Intent intentGobierno = new Intent(this, LeyGobierno.class);
                startActivity(intentGobierno);

                break;

            case R.id.imgAESA: // AESA

                Intent intentAESA = new Intent(this, LeyAESA.class);
                startActivity(intentAESA);

                break;

        }

    }
}
