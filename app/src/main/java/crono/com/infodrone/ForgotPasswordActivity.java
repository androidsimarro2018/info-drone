package crono.com.infodrone;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView txtEmail;
    private Button btnSend;

    private String email;

    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        txtEmail = findViewById(R.id.txtEmail);
        btnSend = findViewById(R.id.btnSendNewPassword);

        btnSend.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        email = txtEmail.getText().toString();

        if(email.equals("")){
            Toast.makeText(this, "Por favor, " +
                    "introduce un email", Toast.LENGTH_SHORT).show();
        }else{

            firebaseAuth = FirebaseAuth.getInstance();
            firebaseAuth.sendPasswordResetEmail(email);

            guardarPreferencias();

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            finish();
            Intent i = new Intent(this, LoginActivity.class);
            startActivity(i);

        }

    }

    public void guardarPreferencias(){
        SharedPreferences preferences = getSharedPreferences("preferences",
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString("emailEnviado", "Email enviado, por favor, revisa tu correo");
        editor.commit();
    }

}
