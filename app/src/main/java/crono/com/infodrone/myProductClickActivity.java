package crono.com.infodrone;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.service.autofill.Dataset;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;

import crono.com.infodrone.controller.ControllerBD;
import crono.com.infodrone.controller.MyCallBack;
import crono.com.infodrone.controller.TiendaAdapter;
import crono.com.infodrone.models.Producto;

public class myProductClickActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etTitle;
    private EditText etDescription;
    private EditText etPrice;
    private EditText etPhone;
    private ImageView imgUpdate;
    private ImageView imgDelete;
    private ImageView imgIcon;
    private Button btnUpdate;
    private Uri path;
    private Producto producto;

    private String id;
    private String price;
    private String title;
    private String description;
    private String phone;
    private String encoded;
    private ControllerBD controllerBD = new ControllerBD();

    DatabaseReference reference;
    String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_click_my_product);

        id = getIntent().getStringExtra("id");
        price = getIntent().getStringExtra("price");
        title = getIntent().getStringExtra("tittle");
        description = getIntent().getStringExtra("description");
        phone = getIntent().getStringExtra("phone");
        encoded = getIntent().getStringExtra("encoded");

        byte[] decodedString = Base64.decode(encoded, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        Bitmap bitmap = Bitmap.createScaledBitmap(decodedByte, 1050, 800, true);

        imgIcon = findViewById(R.id.imgIcon);
        etTitle = findViewById(R.id.etTitle);
        etDescription = findViewById(R.id.etDescription);
        etPrice = findViewById(R.id.etPrice);
        etPhone = findViewById(R.id.etPhone);
        imgUpdate = findViewById(R.id.imgUpdate);
        imgDelete = findViewById(R.id.imgDelete);
        btnUpdate = findViewById(R.id.btnUpdateProduct);

        etTitle.setText(title);
        etDescription.setText(description);
        etPrice.setText(price);
        etPhone.setText(phone);
        imgIcon.setImageBitmap(bitmap);
        //imgIcon.setRotation(180);
        btnUpdate.setVisibility(View.INVISIBLE);

        etTitle.setEnabled(false);
        etDescription.setEnabled(false);
        etPrice.setEnabled(false);
        etPhone.setEnabled(false);

        imgUpdate.setOnClickListener(this);
        imgDelete.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        FirebaseUser user;

        reference = FirebaseDatabase.getInstance().getReference();

        user = FirebaseAuth.getInstance().getCurrentUser();
        userId = user.getUid();

        imgIcon.setOnClickListener(this);
        btnUpdate.setOnClickListener(this);

        etTitle.setEnabled(true);
        etDescription.setEnabled(true);
        etPrice.setEnabled(true);
        etPhone.setEnabled(true);
        btnUpdate.setVisibility(View.VISIBLE);

        if(v.getId() == R.id.imgIcon){ // Botón para cambiar la imagen
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            intent.setType("image/");
            startActivityForResult(intent.createChooser(intent,"Seleccione una aplicación"),10);

        } else if(v.getId() == R.id.btnUpdateProduct){ // Boton de update

            price = etPrice.getText().toString();
            description = etDescription.getText().toString();
            title = etTitle.getText().toString();
            phone = etPhone.getText().toString();
            String encodedImage = "";

            if(path!=null){ // Se ha cambiado la foto
                final InputStream imageStream;
                try {

                    imageStream = getContentResolver().openInputStream(path);
                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                    Bitmap bitmap = Bitmap.createScaledBitmap(selectedImage, 500, 350, true);
                    encodedImage = encodeImage(bitmap);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                producto = new Producto(id,title, description, price, phone, encodedImage);

            }else{ // No se ha cambiado la foto
                producto = new Producto(id,title, description, price, phone, encoded);
            }

            reference.child("users").child(userId).child("articles").child(id).setValue(producto);

            Intent intent = new Intent(this, TiendaActivity.class);
            startActivity(intent);
            finish();

        } else if(v.getId() == R.id.imgDelete){ // Botón para borrar producto

            // Creo mensaje de confirmación
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Estas seguro/a de eliminar el producto?");
            builder.setTitle("Confirmar Eliminación de Producto");
            builder.setPositiveButton("Si", new DialogInterface.OnClickListener() { // Si se presiona aceptar elimino producto de la base de datos
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    reference.child("users").child(userId).child("articles").child(id).removeValue();
                    Intent intent = new Intent(getApplicationContext(), TiendaActivity.class);

                    startActivity(intent);
                    finish();

                }
            });
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() { // Si se presiona cancelar, no se hace nada
                @Override
                public void onClick(DialogInterface dialog, int which) {
                      dialog.cancel();
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode==RESULT_OK){
            path = data.getData();
            imgIcon.setImageURI(path);
            imgIcon.setRotation(180);

        }

    }

    private String encodeImage(Bitmap bm)
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG,100,baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);

        return encImage;
    }

}
