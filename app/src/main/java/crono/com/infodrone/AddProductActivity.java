package crono.com.infodrone;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

import crono.com.infodrone.models.Producto;

public class  AddProductActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView imgAdd;
    private EditText txtTittle;
    private EditText txtDescription;
    private EditText txtPrice;
    private EditText txtPhone;
    private Button btnAddProduct;
    private Uri path;
    private FirebaseUser user;
    private String userId;
    private Producto producto;

    DatabaseReference reference;
    FirebaseStorage storage;
    StorageReference storageReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);

        // Obtengo el usuario
        user = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference();
        userId = user.getUid();

        // Inicio firebase para guardar
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        imgAdd = findViewById(R.id.imgAdd);
        txtTittle = findViewById(R.id.txtTitleNewProduct);
        txtDescription = findViewById(R.id.txtDescriptionNewProduct);
        txtPrice = findViewById(R.id.txtPriceNewProduct);
        txtPhone = findViewById(R.id.txtPhoneNewProduct);
        btnAddProduct = findViewById(R.id.btnAddProduct);

        imgAdd.setOnClickListener(this);
        btnAddProduct.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        if(v.getId() == R.id.imgAdd){ // Click a la imagen
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            intent.setType("image/");
            startActivityForResult(intent.createChooser(intent,"Seleccione una aplicación"),10);
        } else if(v.getId() == R.id.btnAddProduct){ // Subir articulo

            String title = txtTittle.getText().toString();
            String description = txtDescription.getText().toString();
            String price = txtPrice.getText().toString() + " €";
            String phone = txtPhone.getText().toString();
            String encodedImage = "";

            try {
                final InputStream imageStream = getContentResolver().
                        openInputStream(path);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                Bitmap bitmap = Bitmap.createScaledBitmap(selectedImage,
                        400, 350, true);
                encodedImage = encodeImage(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }

            producto = new Producto("",title, description, price,
                    phone, encodedImage);
            // Write
            reference.child("users").child(userId).child("articles")
                    .push().setValue(producto);

            Intent intent = new Intent(this, TiendaActivity.class);
            startActivity(intent);
            finish();

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode==RESULT_OK){
            path = data.getData();
            imgAdd.setImageURI(path);
            //imgAdd.setRotation(90);

        }

    }

    private String encodeImage(Bitmap bm)
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG,100,baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);

        return encImage;
    }

}
