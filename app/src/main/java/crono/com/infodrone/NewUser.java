package crono.com.infodrone;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class NewUser extends AppCompatActivity implements View.OnClickListener {

    private TextView txtName;
    private TextView txtLastname;
    private TextView txtUser;
    private TextView txtPassword;
    private TextView txtNewPassword;
    private String name;
    private String lastName;
    private String pass;
    private String passRepeat;
    private Button btn;
    private String user;
    private FirebaseUser firebaseUser;

    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_user);

        txtName = findViewById(R.id.txtNombre);
        txtLastname = findViewById(R.id.txtApellidos);
        txtUser = findViewById(R.id.txtUsuario);
        txtPassword = findViewById(R.id.txtPassword);
        txtNewPassword = findViewById(R.id.txtPasswordRepeat);

        firebaseAuth = FirebaseAuth.getInstance();

        btn = findViewById(R.id.btnSendNewPassword);

        btn.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        name = txtName.getText().toString();
        lastName = txtName.getText().toString();
        user = txtUser.getText().toString();
        pass = txtPassword.getText().toString();
        passRepeat = txtNewPassword.getText().toString();

        if( name.isEmpty() || user.isEmpty() || pass.isEmpty() || passRepeat.isEmpty() ){ // NOMBRE DE USUARIO VACIO

            Toast.makeText(this, "Por favor, escriba todos los campos correctamente",
                    Toast.LENGTH_LONG).show();

        }else{ // NOMBRE DE USUARIO NO VACIO

            if( pass.equals(passRepeat) && pass.length() >= 6 ){ // CONTRASEÑAS IGUALES

                firebaseAuth.createUserWithEmailAndPassword(user, pass)
                        .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if(task.isSuccessful()){// ok

                                    firebaseUser = FirebaseAuth.getInstance()
                                            .getCurrentUser();

                                        firebaseUser.sendEmailVerification();

                                    guardarPreferencias();

                                    finish();
                                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                    startActivity(intent);

                                }else{ // FALLO AL REGISTRAR EL USUARIO

                                    Toast.makeText(NewUser.this,
                                            "Error!!" +
                                            " No se ha podido registrar al usuario",
                                            Toast.LENGTH_SHORT).show();

                                }
                            }
                        });

            }else { // CONTRASEÑAS DIFERENTES

                Toast.makeText(this, "Las dos contraseñas " +
                                "deben coincidir",
                        Toast.LENGTH_SHORT).show();

            }

        }

    }

    public void guardarPreferencias(){
        SharedPreferences preferences = getSharedPreferences("preferences",
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString("name", name);
        editor.putString("lastName", lastName);
        editor.putString("user", user);
        editor.putString("emailEnviado", "Hemos enviado un correo de verificación," +
                " por favor, entra al mensaje y verifícalo");
        editor.commit();
    }

}
