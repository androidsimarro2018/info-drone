package crono.com.infodrone;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

public class ProductActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView txtPrice;
    private TextView txtTittle;
    private TextView txtDescription;
    private ImageView imgIcon;
    private Button btnChat;

    private String price;
    private String tittle;
    private String description;
    private String phone;
    private String encoded;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_click);

        price = getIntent().getStringExtra("price");
        tittle = getIntent().getStringExtra("tittle");
        description = getIntent().getStringExtra("description");
        encoded = getIntent().getStringExtra("encoded");
        phone = "+34" + getIntent().getStringExtra("phone");

        byte[] decodedString = Base64.decode(encoded, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString,
                0, decodedString.length);
        Bitmap bitmap = Bitmap.createScaledBitmap(decodedByte,
                1050, 800, true);

        btnChat = findViewById(R.id.btnChat);

        btnChat.setOnClickListener(this);

        txtPrice = findViewById(R.id.txtPriceClick);
        txtTittle = findViewById(R.id.txtTittleClick);
        txtDescription = findViewById(R.id.txtDescriptionClick);
        imgIcon = findViewById(R.id.iconClick);

        txtPrice.setText(price);
        txtTittle.setText(tittle);
        txtDescription.setText(description);
        imgIcon.setImageBitmap(bitmap);


    }

    @Override
    public void onClick(View v) { // Click al botón de chat

        try {
            String whatsAppRoot = "http://api.whatsapp.com/";
            String number = "send?phone=" + phone;
            String text = "&text=Hola, me pongo en contacto contigo porque me " +
                    "interesa uno de los productos que tienes a la venta en Info Drone";
            String uri = whatsAppRoot+number+text;

            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(uri));
            startActivity(intent);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(),
                    "Lo siento, parece que no se puede abrir WhatsApp.",
                    Toast.LENGTH_LONG).show();
        }

    }
}
