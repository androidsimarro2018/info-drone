package crono.com.infodrone.models;

import android.graphics.Bitmap;

public class Producto {

    private String id;
    private String price;
    private String title;
    private String description;
    private String phone;
    private String encoded;

    public Producto(String id, String title, String description, String precio) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.price = precio;
    }

    public Producto(String id, String title, String description, String precio, String phone, String encoded) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.price = precio;
        this.phone = phone;
        this.encoded = encoded;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEncoded() {
        return encoded;
    }

    public void setEncoded(String encoded) {
        this.encoded = encoded;
    }

    @Override
    public String toString() {
        return "Producto{" +
                "id='" + id + '\'' +
                ", price='" + price + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", phone='" + phone + '\'' +
                ", encoded='" + encoded + '\'' +
                '}';
    }
}
