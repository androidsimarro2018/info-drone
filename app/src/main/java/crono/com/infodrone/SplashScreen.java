package crono.com.infodrone;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.ProgressBar;

import java.util.Locale;

public class SplashScreen extends AppCompatActivity implements Runnable{

    private Thread hiloProgresBar= new Thread(this);
    private ProgressBar progressBar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        progressBar = findViewById(R.id.progressBar);

        progressBar.setProgress(0);
        progressBar.setMax(100);

        hiloProgresBar.start();

        // Criden al mètode per a que cambie el idioma en el cas de que s'haja modificat
        setAppLocale();

    }

    public void run() {
        Intent i;
        int cont=0;

        try {
            while (progressBar.getProgress()!=100) {
                Thread.sleep(485);
                progressBar.incrementProgressBy(cont);
                cont++;
            }
            i= new Intent(this, LoginActivity.class);
            startActivity(i);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void setAppLocale() {

        SharedPreferences preferencias = PreferenceManager.getDefaultSharedPreferences(this);
        String idioma = preferencias.getString("idioma", "ESP");
        Log.i("Etiqueta de Log", "idioma"+ idioma);
        Resources res = this.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration configuracion = res.getConfiguration();
        configuracion.setLocale(new Locale(idioma.toLowerCase()));
        res.updateConfiguration(configuracion,dm);
    }

}
