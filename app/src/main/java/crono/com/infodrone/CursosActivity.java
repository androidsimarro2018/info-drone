package crono.com.infodrone;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class CursosActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView imgAerocamaras;
    private ImageView imgEagle;
    private ImageView imgProfesional;
    private ImageView imgAvista;
    private Uri uri;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cursos);

        imgAerocamaras = findViewById(R.id.imgAerocamaras);
        imgEagle = findViewById(R.id.imgEagle);
        imgProfesional = findViewById(R.id.imgDroneProfesional);
        imgAvista = findViewById(R.id.imgAvista);

        imgAerocamaras.setOnClickListener(this);
        imgEagle.setOnClickListener(this);
        imgProfesional.setOnClickListener(this);
        imgAvista.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imgAerocamaras: // Aerocamaras

                uri = Uri.parse("https://cursodedrones.es/");
                intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);

                break;

            case R.id.imgEagle: // Image Eagle

                uri = Uri.parse("https://www.eagledron.es/");
                intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);

                break;

            case R.id.imgDroneProfesional: // Drone Profesional

                uri = Uri.parse("https://dronprofesional.com/piloto-de-drones/");
                intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);

                break;

            case R.id.imgAvista: // Avista

                uri = Uri.parse("https://avistadrone.es/cursos-con-drones/");
                intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);

                break;

        }


    }
}
