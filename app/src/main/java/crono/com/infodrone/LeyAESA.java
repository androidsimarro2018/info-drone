package crono.com.infodrone;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;

public class LeyAESA extends AppCompatActivity implements OnPageChangeListener {

    private static final String TAG = LeySeguridad.class.getSimpleName();
    public static final String SAMPLE_FILE = "leyAESA.pdf";
    PDFView pdfView;
    Integer pageNumber = 0;
    String pdfFileName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leyesunica);
        pdfView= (PDFView)findViewById(R.id.pdfView);
        displayFromAsset(SAMPLE_FILE);
    }

    private void displayFromAsset(String assetFileName) {
        pdfFileName = assetFileName;
        pdfView.fromAsset(SAMPLE_FILE)
                .defaultPage(pageNumber)
                .enableSwipe(true)

                .swipeHorizontal(false) // deshabilito que se pueda leer
                // de forma horizontal, ya que no sé como saldría por
                // problemas de pdf
                .onPageChange(this) // actualizo página
                .enableAnnotationRendering(true) // permito anotaciones
                //.onLoad(this)
                .scrollHandle(new DefaultScrollHandle(this)) //Scroll
                .load();
    }

    @Override public void onPageChanged(int page, int pageCount) {
        pageNumber = page;
        setTitle(String.format("%s %s / %s", pdfFileName, page + 1, pageCount));
    }

}