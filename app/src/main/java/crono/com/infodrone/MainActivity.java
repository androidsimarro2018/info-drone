package crono.com.infodrone;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.widget.Toast;

import com.facebook.login.Login;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import crono.com.infodrone.api.ApiClient;
import crono.com.infodrone.api.ApiInterface;
import crono.com.infodrone.models.Article;
import crono.com.infodrone.models.News;
import crono.com.infodrone.models.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, SwipeRefreshLayout.OnRefreshListener{

    public static final String API_KEY = "19a652d298d04073ab07de52fa78eb2e";
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private List<Article> articles = new ArrayList<>();
    private Adapter adapter;
    private String TAG = MainActivity.class.getSimpleName();
    private SwipeRefreshLayout swipeRefreshLayout;

    DatabaseReference reference;
    FirebaseStorage storage;
    StorageReference storageReference;
    private FirebaseUser firebaseUser;
    private String userId;
    private User newUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        newUser = leerPreferencias();

        if(!newUser.getName().equals("")){

            // Obtengo el usuario
            firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
            reference = FirebaseDatabase.getInstance().getReference();
            userId = firebaseUser.getUid();

            // Inicio firebase para guardar
            storage = FirebaseStorage.getInstance();
            storageReference = storage.getReference();

            // Guardo
            reference.child("users").child(userId).child("information").setValue(newUser);

        }

        swipeRefreshLayout = findViewById(R.id.swipe_refrest_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);

        recyclerView = findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(MainActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);

        onLoadingSwiperefresh("dron");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //LoadJson("drone");

    }

    public void LoadJson(final String keyword){

        swipeRefreshLayout.setRefreshing(true);

        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        String country = "es";
        //String language = "mx";
        //String country = Utils.getCountry();
        //System.out.println("Este es el country: " + country);

        Call<News> call;

        if(keyword.length()> 0){ // Si le paso palabra clave, busco por ella

            call = apiInterface.getNewsSearch(keyword, country,"title", API_KEY);

        }else{ // No le he pasado palabra clave, muestro todas las noticias

            call = apiInterface.getNews(country, API_KEY);

        }

        call.enqueue(new Callback<News>() {
            @Override
            public void onResponse(Call<News> call, Response<News> response) {
                if (response.isSuccessful() && response.body().getArticle() != null){ // He leido correctamente, mostramos noticias

                    if(articles.isEmpty()){
                        articles.clear();
                    }
                    // Lanzo adapter para que rellene toda la activity con todas las noticias que le he pasado
                    articles = response.body().getArticle();
                    adapter = new Adapter(articles, MainActivity.this);
                    recyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                    // Lanzo a método que genera un Listener para cuando se pulse sobre una noticia, lanzar un intent a otra pantalla
                    initListener();
                    // Deshabilito opción de refrescar la pantalla mientras estoy cargando datos para que no pueda entrar en bucle infinito
                    swipeRefreshLayout.setRefreshing(false);

                }else{ // No hay noticias, error al leer de Google
                    swipeRefreshLayout.setRefreshing(false);
                    Toast.makeText(MainActivity.this, "No Result!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<News> call, Throwable t) {

                swipeRefreshLayout.setRefreshing(false);

            }
        });

    }

    private void initListener(){ // Se hace click en una notícia

        adapter.setOnItemClickListener(new Adapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(MainActivity.this,
                        NewsDetailActivity.class);

                Article article = articles.get(position);
                intent.putExtra("url", article.getUrl());
                intent.putExtra("title", article.getTitle());
                intent.putExtra("img", article.getUrlToImage());
                intent.putExtra("date", article.getPublishedAt());
                intent.putExtra("source", article.getSource().getName());
                intent.putExtra("author", article.getAuthor());

                startActivity(intent);

            }
        });

    }

    @Override
    public void onBackPressed() { // Volvemos al Login
        /*DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }*/
        Intent intentLogin = new Intent(this, LoginActivity.class);
        startActivity(intentLogin);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) { // Buscador

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        final SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        MenuItem searchMenuItem = menu.findItem(R.id.action_search);

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setQueryHint("Search Latest News...");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if(query.length() > 2){
                    onLoadingSwiperefresh(query);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                onLoadingSwiperefresh(s);
                return false;
            }
        });

        searchMenuItem.getIcon().setVisible(false, false);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) { // Idioma
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intentConfiguration = new Intent(this, Configuration.class);
            startActivity(intentConfiguration);
        }

        return super.onOptionsItemSelected(item);
    }

    public User leerPreferencias(){

        SharedPreferences preferences = getSharedPreferences("preferences", Context.MODE_PRIVATE);
        String name = preferences.getString("name","");
        String lastName = preferences.getString("lastName","");
        String userr = preferences.getString("user","");

        return new User(name, lastName, userr);

    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) { // Menu desplegable de opciones
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (item.getItemId()){

            case R.id.nav_news: // Noticias

                Intent intentNews = new Intent(this, MainActivity.class);
                startActivity(intentNews);

                break;

            case R.id.nav_information: // Información de cursos

                Intent intentCourses = new Intent(this, CursosActivity.class);
                startActivity(intentCourses);

                break;

            case R.id.nav_regulations: // Leyes

                Intent intentRegulations = new Intent(this, LeyesActivity.class);
                startActivity(intentRegulations);

                break;

            case R.id.nav_map: // Mapa

                Intent intentMap = new Intent(this, MapsActivity.class);
                startActivity(intentMap);

                break;

            case R.id.nav_shop: // Productos

                Intent intentShop = new Intent(this, TiendaActivity.class);
                startActivity(intentShop);

                break;

            case R.id.nav_myProducts: // Mis productos

                Intent intentMyProducts = new Intent(this, MyProductsActivity.class);
                startActivity(intentMyProducts);

                break;

            case R.id.nav_addProduct: // Subir Producto

                Intent intentAddProduct = new Intent(this, AddProductActivity.class);
                startActivity(intentAddProduct);

                break;

            case R.id.nav_myAccount: // Mi cuenta

                Intent intentMyAccount = new Intent(this, MyAccountActivity.class);
                startActivity(intentMyAccount);

                break;

            case R.id.nav_closeSession: // Cerrar Sesión

                Intent intentLogin = new Intent(this, LoginActivity.class);
                startActivity(intentLogin);

                break;

                default:

                    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                    drawer.closeDrawer(GravityCompat.START);

        }

        return true;

    }

    @Override
    public void onRefresh() { // Si se actualiza la página

        LoadJson("dron");

    }

    private void onLoadingSwiperefresh(final String keyword){ // Mientras carga el Swipe

        swipeRefreshLayout.post(
                new Runnable() {
                    @Override
                    public void run() {
                        LoadJson(keyword);
                    }
                }
        );

    }



}
