package crono.com.infodrone.controller;

import java.util.ArrayList;

import crono.com.infodrone.models.Producto;

public interface MyCallBack {
    void onCallBackProducto (ArrayList<Producto> list);
}
