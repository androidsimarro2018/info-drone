package crono.com.infodrone.controller;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import crono.com.infodrone.models.Producto;

public class ControllerBD {

    ArrayList<Producto> listaProductos = new ArrayList<>();
    String userId;
    String encoded;

    public ArrayList<Producto> leerBasedeDatos(final MyCallBack myCallBack){

        DatabaseReference reference;
        DatabaseReference usersReference;

        // Firebase
        reference = FirebaseDatabase.getInstance().getReference();
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                DataSnapshot users = dataSnapshot.child("users");

                for(DataSnapshot dsUser: users.getChildren()){
                    DataSnapshot articles = dsUser.child("articles");
                    for(DataSnapshot dsArticles : articles.getChildren()){
                        String description = (String) dsArticles.child("description")
                                .getValue();
                        String encoded = (String) dsArticles.child("encoded").getValue();
                        String price = (String) dsArticles.child("price").getValue();
                        String phone = (String) dsArticles.child("phone").getValue();
                        String title = (String) dsArticles.child("title").getValue();

                        Producto producto = new Producto(dsArticles.getKey(), title,
                                description, price, phone, encoded);

                        listaProductos.add(producto);
                    }
                }
                myCallBack.onCallBackProducto(listaProductos);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("Fallo al leer");
            }
        }); // Fin Firebase
        return listaProductos;
    }

    // Método para leer los productos del usuario logueado
    public ArrayList<Producto> leerBasedeDatosMisProductos(final MyCallBack myCallBack){

        DatabaseReference reference;
        FirebaseUser user;

        user = FirebaseAuth.getInstance().getCurrentUser();
        userId = user.getUid();

        // Firebase
        reference = FirebaseDatabase.getInstance().getReference();

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                DataSnapshot users = dataSnapshot.child("users");

                DataSnapshot currentUser = users.child(userId);

                DataSnapshot articles = currentUser.child("articles");

                for(DataSnapshot dsArticles : articles.getChildren()){

                    String description = (String) dsArticles.child("description").getValue();
                    String encoded = (String) dsArticles.child("encoded").getValue();
                    String price = (String) dsArticles.child("price").getValue();
                    String phone = (String) dsArticles.child("phone").getValue();
                    String title = (String) dsArticles.child("title").getValue();

                    Producto producto = new Producto(dsArticles.getKey(), title, description, price, phone, encoded);

                    listaProductos.add(producto);

                }

                myCallBack.onCallBackProducto(listaProductos);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("Fallo al leer");
            }
        }); // Fin Firebase

        return listaProductos;

    }

    public String leerEncoded(final MyCallBack myCallBack, final String id){

        DatabaseReference reference;
        FirebaseUser user;

        user = FirebaseAuth.getInstance().getCurrentUser();
        userId = user.getUid();

        // Firebase
        reference = FirebaseDatabase.getInstance().getReference();

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                DataSnapshot users = dataSnapshot.child("users");

                DataSnapshot currentUser = users.child(userId);

                DataSnapshot articles = currentUser.child("articles").child(id);

                System.out.println("Esto es el id en controller: " + id);

                encoded = (String) articles.child("encoded").getValue();

                System.out.println("esto es el encoded en controller: " + encoded);

                //myCallBack.onCallBackProducto(listaProductos);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("Fallo al leer");
            }
        }); // Fin Firebase

        return encoded;

    }


}
