package crono.com.infodrone.controller;


import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import crono.com.infodrone.R;
import crono.com.infodrone.models.Producto;

public class TiendaAdapter<T> extends ArrayAdapter<T> {

    public TiendaAdapter( Context context, ArrayList<T> objects) {
        super(context, 0, objects);
    }

    @SuppressLint("ResourceType")
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View listItemView = convertView;

        if(listItemView==null){
            listItemView = inflater.inflate(R.layout.activity_product,parent,false);
            listItemView.setBackgroundResource(R.layout.grid_items_border);
        }
        TextView txtTittle=listItemView.findViewById(R.id.txtTittle);
        TextView txtPrice = listItemView.findViewById(R.id.txtPrice);
        ImageView imgIcon = listItemView.findViewById(R.id.icon);

        Producto p = (Producto) getItem(position);
        txtTittle.setText(p.getTitle());
        txtPrice.setText(p.getPrice());

        byte[] decodedString = Base64.decode(p.getEncoded(), Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

        imgIcon.setImageBitmap(decodedByte);
        //imgIcon.setRotation(90);

        return listItemView;
    }

}
